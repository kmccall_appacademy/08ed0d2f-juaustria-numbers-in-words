class Fixnum

  def in_words
    stringified = ''
    tens_hash = {
      90 => 'ninety',
      80 => 'eighty',
      70 => 'seventy',
      60 => 'sixty',
      50 => 'fifty',
      40 => 'forty',
      30 => 'thirty',
      20 => 'twenty'
    }
    teens_hash = {
      19 => 'nineteen',
      18 => 'eighteen',
      17 => 'seventeen',
      16 => 'sixteen',
      15 => 'fifteen',
      14 => 'fourteen',
      13 => 'thirteen',
      12 => 'twelve',
      11 => 'eleven',
      10 => 'ten'
    }
    ones_hash = {
      9 => 'nine',
      8 => 'eight',
      7 => 'seven',
      6 => 'six',
      5 => 'five',
      4 => 'four',
      3 => 'three',
      2 => 'two',
      1 => 'one'
    }
    return 'zero' if zero?

    digit_counter = set_counter

    digit_counter.each do |k, v|
      padding(stringified)
      if k == 'tens'
        if v >= 2
          stringified << tens_hash[v * 10]
        else
          stringified << teens_hash[(v * 10) + (digit_counter['ones'])]
          break
        end
      elsif k == 'ones'
        stringified << ones_hash[v]
      else
        stringified << v.in_words + ' ' + k
      end
    end
    stringified
  end

  def padding(str)
    str << ' ' if str != ''
  end

  def set_counter
    counter = self
    digit_counts = Hash.new(0)
    while counter > 0
      if counter >= 1_000_000_000_000
        digit_counts['trillion'] += 1
        counter -= 1_000_000_000_000
      elsif counter >= 1_000_000_000
        digit_counts['billion'] += 1
        counter -= 1_000_000_000
      elsif counter >= 1_000_000
        digit_counts['million'] += 1
        counter -= 1_000_000
      elsif counter >= 1000
        digit_counts['thousand'] += 1
        counter -= 1000
      elsif counter >= 100
        digit_counts['hundred'] += 1
        counter -= 100
      elsif counter >= 10
        digit_counts['tens'] += 1
        counter -= 10
      else
        digit_counts['ones'] += 1
        counter -= 1
      end
    end
    digit_counts
  end
end
